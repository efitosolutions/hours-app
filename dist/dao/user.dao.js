"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const logging_1 = require("../common/logging");
const application_error_1 = require("../common/application-error");
const user_schema_1 = require("../schemas/user-schema");
var UserDao;
(function (UserDao) {
    const populateOptions = [
        'groups',
        'dealer',
        'photo',
    ];
    async function createUser(data) {
        const existingUser = await getUserByEmail(data.email);
        if (existingUser) {
            throw new application_error_1.ApplicationError("Email Already Exists");
        }
        const user = new user_schema_1.default(data);
        logging_1.AppLogger.info(`New User created, userID: ${user.id}`);
        await user.save();
        return await authenticateUser(data.email, data.password);
    }
    UserDao.createUser = createUser;
    async function getUserByEmail(email) {
        let user = await user_schema_1.default.findOne({ email: email }).populate(populateOptions);
        logging_1.AppLogger.info(`Got user for email, userID: ${user ? user.id : "None"}`);
        return user;
    }
    UserDao.getUserByEmail = getUserByEmail;
    async function getUserById(id) {
        // console.log("dao" + id);
        let user = await user_schema_1.default.findById(id);
        if (!user) {
            throw new application_error_1.ApplicationError("User not found for Id: " + id);
        }
        logging_1.AppLogger.info(`Got user for id, userID: ${user}`);
        return user;
    }
    UserDao.getUserById = getUserById;
    async function updateUser(id, data) {
        await user_schema_1.default.findByIdAndUpdate(id, data);
        logging_1.AppLogger.info(`Updated user by ID ${id}`);
        return getUserById(id);
    }
    UserDao.updateUser = updateUser;
    async function authenticateUser(email, password) {
        const user = await getUserByEmail(email);
        if (user) {
            const isMatch = await user.comparePassword(password);
            if (!isMatch) {
                throw new application_error_1.ApplicationError('Incorrect email/password combination');
            }
            return user.createAccessToken();
        }
        else {
            throw new application_error_1.ApplicationError('Email not found');
        }
    }
    UserDao.authenticateUser = authenticateUser;
    async function comparePasswordToChange(email, password) {
        const user = await getUserByEmail(email);
        if (user) {
            const isMatch = await user.comparePassword(password);
            if (!isMatch) {
                throw new application_error_1.ApplicationError('Incorrect email');
            }
            return true;
        }
    }
    UserDao.comparePasswordToChange = comparePasswordToChange;
    async function getAllUsers() {
        const users = await user_schema_1.default.find({ active: true });
        logging_1.AppLogger.info(`get All Users`);
        return users;
    }
    UserDao.getAllUsers = getAllUsers;
    async function deleteUser(id) {
        logging_1.AppLogger.info(`User deleted by ID ${id}`);
        return user_schema_1.default.findByIdAndRemove(id);
    }
    UserDao.deleteUser = deleteUser;
    async function changePassword(data) {
        const user1 = await getUserById(data.id);
        user1.password = data.password;
        await user1.save();
        return user1;
    }
    UserDao.changePassword = changePassword;
})(UserDao = exports.UserDao || (exports.UserDao = {}));
//# sourceMappingURL=user.dao.js.map