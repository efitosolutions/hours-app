import { StringOrObjectId } from "../common/util";
import { DProject, IProject } from "../models/project-model";
export declare namespace ProjectDao {
    function addProject(data: DProject): Promise<IProject>;
    function getProjectByName(name: string): Promise<IProject | null>;
    function getAllProject(): Promise<IProject[]>;
    function updateProject(id: StringOrObjectId, data: Partial<DProject>): Promise<IProject>;
    function getProjectById(id: StringOrObjectId): Promise<IProject>;
}
