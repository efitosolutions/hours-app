"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const logging_1 = require("../common/logging");
const doc_schema_1 = require("../schemas/doc-schema");
var DocDao;
(function (DocDao) {
    async function filter(options) {
        const conditions = { docType: options.docType, docNbr: options.docNbr };
        if (options.ranges.length) {
            const rangeConditions = [];
            for (let range of options.ranges) {
                const rangeCondition = {};
                if (range.companyNbr) {
                    rangeCondition['companyNbr'] = range.companyNbr;
                }
                if (range.year) {
                    rangeCondition['year'] = range.year;
                }
                rangeConditions.push(rangeCondition);
            }
            conditions['ranges'] = { '$elemMatch': { '$or': rangeConditions } };
        }
        // {'ranges': {'$elemMatch': {'$or': [{'year': 2019}]}}}
        console.log(conditions);
        let docs = await doc_schema_1.default.find(conditions).select("-_id -ranges._id -ranges.dates._id");
        logging_1.AppLogger.info(`Get docs for options: ${options}`);
        return docs;
    }
    DocDao.filter = filter;
})(DocDao = exports.DocDao || (exports.DocDao = {}));
//# sourceMappingURL=doc-dao.js.map