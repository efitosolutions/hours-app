"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const logging_1 = require("../common/logging");
const application_error_1 = require("../common/application-error");
const project_schema_1 = require("../schemas/project-schema");
var ProjectDao;
(function (ProjectDao) {
    const populateOptions = [];
    async function addProject(data) {
        const existingProject = await getProjectByName(data.name);
        if (existingProject) {
            throw new application_error_1.ApplicationError("Project Already Added");
        }
        const project = new project_schema_1.default(data);
        logging_1.AppLogger.info(`New Project added, projectID: ${project.id}`);
        await project.save();
        return project;
    }
    ProjectDao.addProject = addProject;
    async function getProjectByName(name) {
        let project = await project_schema_1.default.findOne({ name: name }).populate(populateOptions);
        logging_1.AppLogger.info(`Got user for email, projectID: ${project ? project.name : "None"}`);
        return project;
    }
    ProjectDao.getProjectByName = getProjectByName;
    async function getAllProject() {
        const project = await project_schema_1.default.find({ active: true });
        logging_1.AppLogger.info(`Got All project`);
        return project;
    }
    ProjectDao.getAllProject = getAllProject;
    async function updateProject(id, data) {
        await project_schema_1.default.findByIdAndUpdate(id, data);
        logging_1.AppLogger.info(`Updated user by ID ${id}`);
        return getProjectById(id);
    }
    ProjectDao.updateProject = updateProject;
    async function getProjectById(id) {
        let project = await project_schema_1.default.findById(id);
        if (!project) {
            throw new application_error_1.ApplicationError("Project not found for Id: " + id);
        }
        logging_1.AppLogger.info(`Got user for id, userID: ${project}`);
        return project;
    }
    ProjectDao.getProjectById = getProjectById;
})(ProjectDao = exports.ProjectDao || (exports.ProjectDao = {}));
//# sourceMappingURL=project-dao.js.map