import { DocFilterOptions, IDoc } from "../models/doc-model";
export declare namespace DocDao {
    function filter(options: DocFilterOptions): Promise<IDoc[]>;
}
