import { DHours, IHours } from "../models/hours-model";
import { StringOrObjectId } from "../common/util";
export declare namespace HoursDao {
    function addHours(data: DHours): Promise<IHours>;
    function getAllHouersDetails(): Promise<IHours[]>;
    function deletehour(id: StringOrObjectId): Promise<IHours>;
    function getProjectHourDetailsById(id: string): Promise<IHours[]>;
    function getUserHourDetailsById(id: string): Promise<IHours[]>;
}
