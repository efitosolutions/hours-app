import { DUser, IUser } from "../models/user-model";
import { StringOrObjectId } from "../common/util";
export declare namespace UserDao {
    function createUser(data: DUser): Promise<string>;
    function getUserByEmail(email: string): Promise<IUser | null>;
    function getUserById(id: StringOrObjectId): Promise<IUser>;
    function updateUser(id: StringOrObjectId, data: Partial<DUser>): Promise<IUser>;
    function authenticateUser(email: string, password: string): Promise<string>;
    function comparePasswordToChange(email: string, password: string): Promise<boolean>;
    function getAllUsers(): Promise<IUser[]>;
    function deleteUser(id: StringOrObjectId): Promise<IUser>;
    function changePassword(data: DUser): Promise<IUser>;
}
