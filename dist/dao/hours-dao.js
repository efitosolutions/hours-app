"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const hours_schemas_1 = require("../schemas/hours-schemas");
const logging_1 = require("../common/logging");
var HoursDao;
(function (HoursDao) {
    const populateOptions = [
        "user",
        "project"
    ];
    async function addHours(data) {
        const hour = new hours_schemas_1.default(data);
        logging_1.AppLogger.info(`New User created, hourID: ${hour.id}`);
        await hour.save();
        return hour;
    }
    HoursDao.addHours = addHours;
    async function getAllHouersDetails() {
        const hours = await hours_schemas_1.default.find().sort({ date: -1 }).populate(populateOptions);
        logging_1.AppLogger.info(`get All Hours Details`);
        return hours;
    }
    HoursDao.getAllHouersDetails = getAllHouersDetails;
    async function deletehour(id) {
        logging_1.AppLogger.info(`Hour deleted by ID ${id}`);
        return hours_schemas_1.default.findByIdAndRemove(id);
    }
    HoursDao.deletehour = deletehour;
    async function getProjectHourDetailsById(id) {
        const hours = await hours_schemas_1.default.find({ project: id }).sort({ date: -1 }).populate(populateOptions);
        logging_1.AppLogger.info(`get All Hours Details`);
        return hours;
    }
    HoursDao.getProjectHourDetailsById = getProjectHourDetailsById;
    async function getUserHourDetailsById(id) {
        const hours = await hours_schemas_1.default.find({ user: id }).sort({ date: -1 }).populate(populateOptions);
        logging_1.AppLogger.info(`get All Hours Details`);
        return hours;
    }
    HoursDao.getUserHourDetailsById = getUserHourDetailsById;
})(HoursDao = exports.HoursDao || (exports.HoursDao = {}));
//# sourceMappingURL=hours-dao.js.map