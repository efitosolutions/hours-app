import { Response } from "express";
import * as mongoose from "mongoose";
export declare type ObjectIdOr<T extends mongoose.Document> = mongoose.Types.ObjectId | T;
export declare type StringOrObjectId = string | mongoose.Types.ObjectId;
export declare namespace Util {
    function sendSuccess(res: Response, data: any, message?: any): void;
    function sendError(res: Response, error: any, errorCode?: number): void;
}
