export declare class ApplicationError extends Error {
    constructor(msg: string);
}
