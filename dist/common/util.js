"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const logging_1 = require("./logging");
var Util;
(function (Util) {
    function sendSuccess(res, data, message) {
        res.send({ success: true, data: data, message: message });
    }
    Util.sendSuccess = sendSuccess;
    function sendError(res, error, errorCode = 0) {
        if (typeof error === 'string') {
            res.send({ success: false, error: error, errorCode: errorCode });
        }
        else {
            if (!error) {
                error = { stack: null, message: "Unknown Error" };
            }
            logging_1.ErrorLogger.error(error.stack);
            res.send({ success: false, data: error, message: error.message, errorCode: errorCode });
        }
    }
    Util.sendError = sendError;
})(Util = exports.Util || (exports.Util = {}));
//# sourceMappingURL=util.js.map