import * as mongoose from "mongoose";
import { IProject } from "../models/project-model";
export declare const projectSchema: mongoose.Schema<any>;
declare const Project: mongoose.Model<IProject, {}>;
export default Project;
