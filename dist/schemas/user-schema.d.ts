import * as mongoose from "mongoose";
import { IUser } from "../models/user-model";
export declare const userSchema: mongoose.Schema<any>;
declare const User: mongoose.Model<IUser, {}>;
export default User;
