"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const mongoose_1 = require("mongoose");
const user_schema_1 = require("./user-schema");
const project_schema_1 = require("./project-schema");
exports.hoursSchema = new mongoose.Schema({
    user: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: user_schema_1.default.modelName,
        required: true,
    },
    project: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: project_schema_1.default.modelName,
        required: true,
    },
    hours: {
        type: mongoose_1.Schema.Types.Number,
        required: true,
    },
    date: {
        type: mongoose_1.Schema.Types.Date,
        required: true,
    },
    note: {
        type: mongoose_1.Schema.Types.String,
        required: false,
    }
});
const Hours = mongoose.model('Hours', exports.hoursSchema);
exports.default = Hours;
//# sourceMappingURL=hours-schemas.js.map