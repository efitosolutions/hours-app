"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const mongoose_1 = require("mongoose");
const bcrypt = require("bcryptjs");
const jwt = require('jsonwebtoken');
exports.userSchema = new mongoose.Schema({
    name: {
        type: mongoose_1.Schema.Types.String,
        required: true,
    },
    email: {
        type: mongoose_1.Schema.Types.String,
        unique: true,
        required: true,
    },
    password: {
        type: mongoose_1.Schema.Types.String,
        required: true,
    },
    number: {
        type: mongoose_1.Schema.Types.Number,
        required: true,
    },
    role: {
        type: mongoose_1.Schema.Types.String,
        required: true,
    },
    active: {
        type: mongoose_1.Schema.Types.Boolean,
        required: true,
    }
}, {
    toJSON: {
        virtuals: true,
        transform: (doc, ret) => {
            delete ret._id;
            delete ret.password;
        }
    },
});
exports.userSchema.pre('save', function (next) {
    const user = this;
    // only hash the password if it has been modified (or is new)
    if (!user.isModified('password'))
        return next();
    // generate a salt
    // noinspection JSIgnoredPromiseFromCall
    bcrypt.genSalt(10, function (err, salt) {
        if (err)
            return next(err);
        // hash the password using our new salt
        // noinspection JSIgnoredPromiseFromCall
        bcrypt.hash(user.password, salt, function (err, hash) {
            if (err)
                return next(err);
            user.password = hash;
            next();
        });
    });
});
exports.userSchema.methods.createAccessToken = function () {
    return jwt.sign({ user_id: this.id }, process.env.JWT_SECRET);
};
exports.userSchema.methods.comparePassword = function (password) {
    return new Promise((resolve, reject) => {
        // noinspection JSIgnoredPromiseFromCall
        bcrypt.compare(password, this.password, function (err, isMatch) {
            if (err) {
                return reject(err);
            }
            return resolve(isMatch);
        });
    });
};
const User = mongoose.model('User', exports.userSchema);
exports.default = User;
//# sourceMappingURL=user-schema.js.map