import * as mongoose from "mongoose";
import { IDoc } from "../models/doc-model";
export declare const nbrDateSchema: mongoose.Schema<any>;
export declare const nbrSchema: mongoose.Schema<any>;
export declare const docSchema: mongoose.Schema<any>;
declare const Doc: mongoose.Model<IDoc, {}>;
export default Doc;
