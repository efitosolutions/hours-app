"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const mongoose_1 = require("mongoose");
exports.projectSchema = new mongoose.Schema({
    name: {
        type: mongoose_1.Schema.Types.String,
        required: true,
    },
    client: {
        type: mongoose_1.Schema.Types.String,
        unique: true,
        required: true,
    },
    active: {
        type: mongoose_1.Schema.Types.Boolean,
        unique: true,
        required: true,
    }
});
const Project = mongoose.model('Project', exports.projectSchema);
exports.default = Project;
//# sourceMappingURL=project-schema.js.map