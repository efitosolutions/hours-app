"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const mongoose_1 = require("mongoose");
const config_1 = require("../config");
exports.nbrDateSchema = new mongoose.Schema({
    period: {
        type: mongoose_1.Schema.Types.Number,
        required: true,
    },
    startDate: {
        type: mongoose_1.Schema.Types.Date,
        required: true,
    },
    endDate: {
        type: mongoose_1.Schema.Types.Date,
        required: true,
    },
}, config_1.schemaOptions);
exports.nbrSchema = new mongoose.Schema({
    companyNbr: {
        type: mongoose_1.Schema.Types.String,
        required: false,
    },
    year: {
        type: mongoose_1.Schema.Types.Number,
        required: true,
    },
    dates: [{
            type: exports.nbrDateSchema,
            required: true,
            default: [],
        }],
}, config_1.schemaOptions);
exports.docSchema = new mongoose.Schema({
    docType: {
        type: mongoose_1.Schema.Types.String,
        required: false,
    },
    docNbr: {
        type: mongoose_1.Schema.Types.Number,
        required: true,
    },
    ranges: [{
            type: exports.nbrSchema,
            required: true,
            default: [],
        }],
}, config_1.schemaOptions);
const Doc = mongoose.model('Doc', exports.docSchema);
exports.default = Doc;
//# sourceMappingURL=doc-schema.js.map