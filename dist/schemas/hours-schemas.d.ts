import * as mongoose from "mongoose";
import { IHours } from "../models/hours-model";
export declare const hoursSchema: mongoose.Schema<any>;
declare const Hours: mongoose.Model<IHours, {}>;
export default Hours;
