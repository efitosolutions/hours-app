"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.schemaOptions = {
    _id: true,
    id: false,
    toJSON: { getters: true },
    timestamps: false,
    skipVersioning: true
};
exports.uploadPath = process.env.UPLOAD_PATH;
//# sourceMappingURL=config.js.map