"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const user_ep_1 = require("../end-points/user-ep");
const project_ep_1 = require("../end-points/project-ep");
const hours_ep_1 = require("../end-points/hours-ep");
function initAuthRoutes(app) {
    /* AUTH ROUTES */
    app.get('/api/auth/user', user_ep_1.UserEp.getSelf);
    // app.post('/api/auth/common/user/update', UserEp.updateUser);
    app.post('/api/auth/user/update-user', user_ep_1.UserEp.updateUser);
    app.get('/api/auth/user/get-profile', user_ep_1.UserEp.getProfile);
    app.post('/api/auth/user/add-hours', hours_ep_1.HoursEp.addHours);
    app.post('/api/auth/user/get-user', user_ep_1.UserEp.getUserById);
    app.post('/api/auth/user/delete-hour', hours_ep_1.HoursEp.deleteHour);
    app.post('/api/auth/user/project-hour-detail', hours_ep_1.HoursEp.getProjectHourDetailsById);
    app.post('/api/auth/user/compare-password', user_ep_1.UserEp.comparePasswordToChange1);
    app.post('/api/auth/user/change-password', user_ep_1.UserEp.changePassword);
    // app.post('/api/auth/user/project-by-name', ProjectEp.getProjectByName);
    /* ADMIN ROUTES */
    app.get('/api/auth/admin/get-all-users', user_ep_1.UserEp.getAllUsers);
    app.post('/api/auth/admin/register', user_ep_1.UserEp.register);
    app.post('/api/auth/admin/delete-user', user_ep_1.UserEp.deleteUser);
    // app.post('/api/auth/admin/update-user', UserEp.updateUser);
    app.post('/api/auth/admin/add-project', project_ep_1.ProjectEp.addProject);
    app.post('/api/auth/admin/update-project', project_ep_1.ProjectEp.updateProject);
    app.post('/api/auth/admin/user-hour-detail', hours_ep_1.HoursEp.getUserHourDetailsById);
    /* PUBLIC ROUTES */
    app.post('/api/public/login', user_ep_1.UserEp.authenticate);
    app.post('/api/public/email-exists', user_ep_1.UserEp.isEmailExists);
    app.post('/api/public/add-project', project_ep_1.ProjectEp.addProject);
    app.get('/api/public/get-hours', hours_ep_1.HoursEp.getAllHourseDetails);
    app.get('/api/public/get-project', project_ep_1.ProjectEp.getAllProject);
    // app.post('/api/public/delete-user', UserEp.deleteUser);
}
exports.initAuthRoutes = initAuthRoutes;
//# sourceMappingURL=auth.js.map