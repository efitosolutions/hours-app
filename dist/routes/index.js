"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const util_1 = require("../common/util");
const auth_1 = require("./auth");
const basic_1 = require("./basic");
function initRoutes(app) {
    /* TOP LEVEL */
    app.get('/api', (req, res) => util_1.Util.sendSuccess(res, "Example™ Api"));
    auth_1.initAuthRoutes(app);
    basic_1.initBasicRoutes(app);
    /* ALL INVALID REQUESTS */
    app.all('*', (req, res) => util_1.Util.sendError(res, "Route Not Found"));
}
exports.initRoutes = initRoutes;
//# sourceMappingURL=index.js.map