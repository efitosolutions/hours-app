"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const doc_ep_1 = require("../end-points/doc-ep");
function initBasicRoutes(app) {
    /* AUTH ROUTES */
    app.post('/api/public/filter-docs', doc_ep_1.DocEp.docFilterValidationRules(), doc_ep_1.DocEp.filterDoc);
}
exports.initBasicRoutes = initBasicRoutes;
//# sourceMappingURL=basic.js.map