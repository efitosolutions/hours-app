"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const doc_dao_1 = require("../dao/doc-dao");
const util_1 = require("../common/util");
const express_validator_1 = require("express-validator");
var DocEp;
(function (DocEp) {
    function docFilterValidationRules() {
        return [
            express_validator_1.body('docType').isString().escape(),
            express_validator_1.body('docNbr').isNumeric().escape(),
            express_validator_1.body('ranges.*.companyNbr').optional({ checkFalsy: true }).escape(),
            express_validator_1.body('ranges.*.year').optional({ checkFalsy: true }).isNumeric().escape(),
        ];
    }
    DocEp.docFilterValidationRules = docFilterValidationRules;
    function filterDoc(req, res, next) {
        const errors = express_validator_1.validationResult(req);
        if (!errors.isEmpty()) {
            return util_1.Util.sendError(res, { errors: errors.array() });
        }
        doc_dao_1.DocDao.filter(req.body).then(docs => {
            util_1.Util.sendSuccess(res, docs.map((e) => e.toJSON()));
        }).catch(next);
    }
    DocEp.filterDoc = filterDoc;
})(DocEp = exports.DocEp || (exports.DocEp = {}));
//# sourceMappingURL=doc-ep.js.map