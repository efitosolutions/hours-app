import { NextFunction, Request, Response } from "express";
export declare namespace ProjectEp {
    function addProject(req: Request, res: Response, next: NextFunction): Promise<void>;
    function getAllProject(req: Request, res: Response): Promise<void>;
    function updateProject(req: Request, res: Response): Promise<void>;
}
