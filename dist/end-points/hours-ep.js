"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const util_1 = require("../common/util");
const hours_dao_1 = require("../dao/hours-dao");
var HoursEp;
(function (HoursEp) {
    async function addHours(req, res) {
        //console.log(req)
        // TODO validate data;
        const hoursData = req.body;
        const hours = await hours_dao_1.HoursDao.addHours(hoursData);
        util_1.Util.sendSuccess(res, hours);
    }
    HoursEp.addHours = addHours;
    async function getAllHourseDetails(req, res) {
        // TODO validate data;
        const hours = await hours_dao_1.HoursDao.getAllHouersDetails();
        util_1.Util.sendSuccess(res, hours);
    }
    HoursEp.getAllHourseDetails = getAllHourseDetails;
    async function deleteHour(req, res) {
        // console.log(req.body.userId);
        let hourDeleted = hours_dao_1.HoursDao.deletehour(req.body.hourId);
        if (hourDeleted) {
            util_1.Util.sendSuccess(res, "Hour Deleted!");
        }
        else {
            util_1.Util.sendError(res, "Hour Not Deleted");
        }
    }
    HoursEp.deleteHour = deleteHour;
    async function getProjectHourDetailsById(req, res) {
        // TODO validate data;
        const hours = await hours_dao_1.HoursDao.getProjectHourDetailsById(req.body.hourProjectId);
        util_1.Util.sendSuccess(res, hours);
    }
    HoursEp.getProjectHourDetailsById = getProjectHourDetailsById;
    async function getUserHourDetailsById(req, res) {
        // TODO validate data;
        const hours = await hours_dao_1.HoursDao.getUserHourDetailsById(req.body.hourUserId);
        util_1.Util.sendSuccess(res, hours);
    }
    HoursEp.getUserHourDetailsById = getUserHourDetailsById;
})(HoursEp = exports.HoursEp || (exports.HoursEp = {}));
//# sourceMappingURL=hours-ep.js.map