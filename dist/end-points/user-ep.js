"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const util_1 = require("../common/util");
const user_dao_1 = require("../dao/user.dao");
var UserEp;
(function (UserEp) {
    async function authenticate(req, res, next) {
        // TODO validate data;
        user_dao_1.UserDao.authenticateUser(req.body.email, req.body.password).then(token => {
            util_1.Util.sendSuccess(res, token);
        }).catch(next);
    }
    UserEp.authenticate = authenticate;
    async function comparePasswordToChange1(req, res, next) {
        // TODO validate data;
        user_dao_1.UserDao.comparePasswordToChange(req.body.email, req.body.oldPassword).then(password => {
            // console.log(password);
            util_1.Util.sendSuccess(res, password);
        }).catch(next);
    }
    UserEp.comparePasswordToChange1 = comparePasswordToChange1;
    async function isEmailExists(req, res) {
        // TODO validate data;
        const user = await user_dao_1.UserDao.getUserByEmail(req.body.email);
        util_1.Util.sendSuccess(res, !!user);
    }
    UserEp.isEmailExists = isEmailExists;
    async function register(req, res, next) {
        // TODO validate data;
        const userData = req.body;
        await user_dao_1.UserDao.createUser(userData).then((user) => {
            util_1.Util.sendSuccess(res, user);
        }).catch(next);
    }
    UserEp.register = register;
    function getSelf(req, res, next) {
        // noinspection TypeScriptUnresolvedVariable
        user_dao_1.UserDao.getUserById(req.user._id).then(user => {
            util_1.Util.sendSuccess(res, user);
        }).catch(next);
    }
    UserEp.getSelf = getSelf;
    async function updateUser(req, res) {
        try {
            const user = req.body;
            const profile = await user_dao_1.UserDao.updateUser(user.id, req.body);
            util_1.Util.sendSuccess(res, profile);
        }
        catch (e) {
            util_1.Util.sendError(res, e);
        }
    }
    UserEp.updateUser = updateUser;
    async function getAllUsers(req, res) {
        // TODO validate data;
        const users = await user_dao_1.UserDao.getAllUsers();
        util_1.Util.sendSuccess(res, users, "All users returned");
    }
    UserEp.getAllUsers = getAllUsers;
    async function getProfile(req, res) {
        util_1.Util.sendSuccess(res, req.user);
    }
    UserEp.getProfile = getProfile;
    async function deleteUser(req, res) {
        // console.log(req.body.userId);
        let userDeleted = user_dao_1.UserDao.deleteUser(req.body.userId);
        if (userDeleted) {
            util_1.Util.sendSuccess(res, "User Deleted!");
        }
        else {
            util_1.Util.sendError(res, "User Not Deleted");
        }
    }
    UserEp.deleteUser = deleteUser;
    async function getUserById(req, res) {
        // console.log("back" + req.body.id);
        let user = await user_dao_1.UserDao.getUserById(req.body.id);
        // console.log(user);
        util_1.Util.sendSuccess(res, user, "user returned");
    }
    UserEp.getUserById = getUserById;
    async function changePassword(req, res) {
        // console.log("enpppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppp"+req.body.password);
        let user = await user_dao_1.UserDao.changePassword(req.body);
        util_1.Util.sendSuccess(res, user, "Password Change");
    }
    UserEp.changePassword = changePassword;
})(UserEp = exports.UserEp || (exports.UserEp = {}));
//# sourceMappingURL=user-ep.js.map