import { NextFunction, Request, Response } from "express";
export declare namespace UserEp {
    function authenticate(req: Request, res: Response, next: NextFunction): Promise<void>;
    function comparePasswordToChange1(req: Request, res: Response, next: NextFunction): Promise<void>;
    function isEmailExists(req: Request, res: Response): Promise<void>;
    function register(req: Request, res: Response, next: NextFunction): Promise<void>;
    function getSelf(req: Request, res: Response, next: NextFunction): void;
    function updateUser(req: Request, res: Response): Promise<void>;
    function getAllUsers(req: Request, res: Response): Promise<void>;
    function getProfile(req: Request, res: Response): Promise<void>;
    function deleteUser(req: Request, res: Response): Promise<void>;
    function getUserById(req: Request, res: Response): Promise<void>;
    function changePassword(req: Request, res: Response): Promise<void>;
}
