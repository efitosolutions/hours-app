import { NextFunction, Request, Response } from "express";
export declare namespace DocEp {
    function docFilterValidationRules(): import("express-validator").ValidationChain[];
    function filterDoc(req: Request, res: Response, next: NextFunction): void;
}
