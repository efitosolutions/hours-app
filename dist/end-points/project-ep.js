"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const util_1 = require("../common/util");
const project_dao_1 = require("../dao/project-dao");
var ProjectEp;
(function (ProjectEp) {
    // export async function isProjectExists(req: Request, res: Response) {
    //     // TODO validate data;
    //     const project = await ProjectDao.getProjectByName(req.body.name);
    //     Util.sendSuccess(res, !!project);
    // }
    async function addProject(req, res, next) {
        // TODO validate data;
        const projectData = req.body;
        project_dao_1.ProjectDao.addProject(projectData).then(project => {
            util_1.Util.sendSuccess(res, project);
        }).catch(next);
    }
    ProjectEp.addProject = addProject;
    async function getAllProject(req, res) {
        const project = await project_dao_1.ProjectDao.getAllProject();
        util_1.Util.sendSuccess(res, project);
    }
    ProjectEp.getAllProject = getAllProject;
    async function updateProject(req, res) {
        try {
            const project = req.body;
            const profiledetails = await project_dao_1.ProjectDao.updateProject(project._id, req.body);
            util_1.Util.sendSuccess(res, profiledetails);
        }
        catch (e) {
            util_1.Util.sendError(res, e);
        }
    }
    ProjectEp.updateProject = updateProject;
    // export async function getProjectByName(req: Request, res: Response) {
    //     console.log("ddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd");
    //     console.log(req.body);
    //     // const projectName:string = req.body.projectName;
    //     // const project = await ProjectDao.getProjectByName(projectName);
    //     // Util.sendSuccess(res, project);
    // }
})(ProjectEp = exports.ProjectEp || (exports.ProjectEp = {}));
//# sourceMappingURL=project-ep.js.map