import { Request, Response } from "express";
export declare namespace HoursEp {
    function addHours(req: Request, res: Response): Promise<void>;
    function getAllHourseDetails(req: Request, res: Response): Promise<void>;
    function deleteHour(req: Request, res: Response): Promise<void>;
    function getProjectHourDetailsById(req: Request, res: Response): Promise<void>;
    function getUserHourDetailsById(req: Request, res: Response): Promise<void>;
}
