"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const passport = require("passport");
const passport_jwt_1 = require("passport-jwt");
const logging_1 = require("../common/logging");
const user_schema_1 = require("../schemas/user-schema");
const passportJWT = require("passport-jwt");
const LocalStrategy = require('passport-local').Strategy;
const JWTStrategy = passportJWT.Strategy;
async function passportStartup(app) {
    app.use(passport.initialize());
    app.use(passport.session());
    passport.use(new LocalStrategy({
        usernameField: 'email',
        passwordField: 'password'
    }, (username, password, callback) => {
        return user_schema_1.default.findOne({ email: username }).then((user) => {
            if (!user) {
                return callback(null, { message: 'Incorrect username/password combination' });
            }
        }).catch((ex) => {
            logging_1.ErrorLogger.error(ex);
            return callback(ex);
        });
    }));
    passport.use(new JWTStrategy({
        jwtFromRequest: passport_jwt_1.ExtractJwt.fromAuthHeaderAsBearerToken(),
        secretOrKey: process.env.JWT_SECRET
    }, (jwtPayload, callback) => {
        return user_schema_1.default.findById(jwtPayload.user_id).then((user) => {
            return callback(null, user);
        }).catch((ex) => {
            return callback(ex);
        });
    }));
}
exports.default = passportStartup;
//# sourceMappingURL=passport.js.map