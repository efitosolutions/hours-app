"use strict";
// import * as mongoose from 'mongoose';
// import {Schema} from "mongoose";
Object.defineProperty(exports, "__esModule", { value: true });
// const object = Schema.Types.ObjectId;
// object.prototype.get();
// mongoose.SchemaTypes.ObjectId.prototype.get((v: any) => v.toString());
const mongoose = require('mongoose');
mongoose.ObjectId.get((v) => v ? v.toString() : v);
async function databaseSetup() {
    await mongoose.connect(process.env.MONGOOSE_URI, {
        useNewUrlParser: true,
    });
}
exports.default = databaseSetup;
//# sourceMappingURL=database.js.map