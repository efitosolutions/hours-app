import * as express from 'express';
export default function passportStartup(app: express.Application): Promise<void>;
