"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/// <reference path="global.d.ts" />
const cors = require('cors');
const express = require("express");
const fs = require("fs");
const https = require("https");
const morgan = require("morgan");
const routes = require("./routes");
const body_parser_1 = require("body-parser");
const request_logger_1 = require("./middleware/request-logger");
const error_handler_1 = require("./middleware/error-handler");
const authentication_1 = require("./middleware/authentication");
const logging_1 = require("./common/logging");
const database_1 = require("./startup/database");
const passport_1 = require("./startup/passport");
// import * as cors from 'cors';
require('dotenv').config();
// console.clear();
const production = process.env.NODE_ENV === "production";
const PORT = process.env.PORT || 4000;
// noinspection JSIgnoredPromiseFromCall
database_1.default();
const app = express();
app.use(request_logger_1.logRequest);
app.use(express.json());
app.use(body_parser_1.urlencoded({ extended: true }));
// noinspection JSIgnoredPromiseFromCall
passport_1.default(app);
app.use(morgan('combined'));
if (!production) {
    app.use(cors({
        optionsSuccessStatus: 200,
        origin: '*',
        allowedHeaders: ['Content-Type, Access-Control-Allow-Headers, Access-Control-Allow-Origin, Authorization, X-Requested-With']
    }));
}
app.use('/api/auth', authentication_1.Authentication.verifyToken);
app.use('/api/auth/admin', authentication_1.Authentication.verifyAdmin);
if (production) {
    https.createServer({
        key: fs.readFileSync(process.env.SERVER_KEY_PATH || 'server.key'),
        cert: fs.readFileSync(process.env.SERVER_CERT_PATH || 'server.cert')
    }, app).listen(PORT, () => {
        logging_1.AppLogger.info('--> HTTPS Server successfully started at port ' + PORT);
    });
}
else {
    app.listen(PORT, () => {
        logging_1.AppLogger.info('--> Server successfully started at port ' + PORT);
    });
}
routes.initRoutes(app);
app.use(error_handler_1.handleError);
app.use(cors());
exports.default = app;
//# sourceMappingURL=server.js.map