"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const logging_1 = require("../common/logging");
const application_error_1 = require("../common/application-error");
const mongoose = require("mongoose");
const util_1 = require("../common/util");
function handleError(error, req, res, next) {
    logging_1.AppLogger.error(error.message);
    if (error instanceof application_error_1.ApplicationError) {
        util_1.Util.sendError(res, error.message);
    }
    else if (error instanceof mongoose.Error) {
        util_1.Util.sendError(res, error.message, 1);
    }
    else {
        logging_1.ErrorLogger.error(error.stack);
        util_1.Util.sendError(res, "An internal server error occurred", 1);
    }
}
exports.handleError = handleError;
//# sourceMappingURL=error-handler.js.map