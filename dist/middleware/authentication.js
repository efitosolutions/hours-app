"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const passport = require("passport");
const logging_1 = require("../common/logging");
const util_1 = require("../common/util");
class Authentication {
    static verifyToken(req, res, next) {
        return passport.authenticate('jwt', { session: false }, (err, user, info) => {
            if (err || !user) {
                logging_1.AppLogger.error(`Login Failed. reason: ${info}`);
                return util_1.Util.sendError(res, info);
            }
            req.user = user;
            req.body.user = user.id;
            return next();
        })(req, res, next);
    }
    static verifyAdmin(req, res, next) {
        if (req.user.role == 'admin') {
            return next();
        }
        else {
            const info = 'Your not admin';
            logging_1.AppLogger.error(`Login Failed. reason: ${info}`);
            return util_1.Util.sendError(res, info);
        }
    }
}
exports.Authentication = Authentication;
//# sourceMappingURL=authentication.js.map