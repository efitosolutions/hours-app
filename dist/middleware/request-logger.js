"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const logging_1 = require("../common/logging");
function logRequest(req, res, next) {
    if (req.method !== "OPTIONS") {
        logging_1.UaLogger.info(req.headers['user-agent'] + " :: " + req.url);
        res.on('finish', () => {
            const user = (req.user && req.user._id) ? 'U=' + req.user._id.toString() : "";
            const resOut = `${user.padEnd(6)} ${res.statusCode} ${req.method.padEnd(7)} ${req.url} ${res.statusMessage}`;
            logging_1.RequestLogger.info(resOut);
        });
    }
    next();
}
exports.logRequest = logRequest;
//# sourceMappingURL=request-logger.js.map