import { NextFunction, Request, Response } from "express";
export declare class Authentication {
    static verifyToken(req: Request, res: Response, next: NextFunction): any;
    static verifyAdmin(req: Request, res: Response, next: NextFunction): void;
}
