import { NextFunction, Request, Response } from 'express';
export declare function handleError(error: Error, req: Request, res: Response, next: NextFunction): void;
