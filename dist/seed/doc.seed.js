"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const doc_schema_1 = require("../schemas/doc-schema");
const fs = require('fs');
async function seedDocs() {
    let docs = await doc_schema_1.default.find({});
    if (docs.length) {
        return docs;
    }
    let docsFile = fs.readFileSync('src/seed/data/docs.json');
    let docsJson = JSON.parse(docsFile);
    const doc = await doc_schema_1.default.create(docsJson);
    return [doc];
}
exports.default = seedDocs;
//# sourceMappingURL=doc.seed.js.map