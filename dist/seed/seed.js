"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const user_seed_1 = require("./user.seed");
const project_seed_1 = require("./project-seed");
// import seedDocs from "./doc.seed";
async function seed() {
    const users = await user_seed_1.default();
    const projects = await project_seed_1.default();
}
exports.default = seed;
//# sourceMappingURL=seed.js.map