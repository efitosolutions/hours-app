"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const user_schema_1 = require("../schemas/user-schema");
async function databaseClear() {
    await user_schema_1.default.deleteMany({});
}
exports.default = databaseClear;
//# sourceMappingURL=database-clear.js.map