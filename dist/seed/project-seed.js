"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const project_dao_1 = require("../dao/project-dao");
async function seedProject() {
    const project = {
        name: `project2`,
        client: "client2",
        active: true,
    };
    let project1;
    try {
        project1 = await createProject(project);
    }
    catch (e) {
        console.log(e);
    }
    return [project1];
}
exports.default = seedProject;
async function createProject(project) {
    return await project_dao_1.ProjectDao.addProject(project);
}
//# sourceMappingURL=project-seed.js.map