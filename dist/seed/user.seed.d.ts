export declare const adminEmail = "admin@test.com";
export default function seedUsers(): Promise<(string | import("../models/user-model").IUser)[]>;
