"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var RandUtil;
(function (RandUtil) {
    function getRandElements(list) {
        const min = getRandomArbitrary(0, list.length - Math.min(list.length, 8));
        const size = getRandomArbitrary(Math.min(list.length, 2), Math.min(list.length, 8));
        return list.slice(min, size);
    }
    RandUtil.getRandElements = getRandElements;
    function getRandomArbitrary(min, max) {
        return Math.round(Math.random() * (max - min) + min);
    }
    RandUtil.getRandomArbitrary = getRandomArbitrary;
    function randomDate(start, end) {
        return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
    }
    RandUtil.randomDate = randomDate;
})(RandUtil = exports.RandUtil || (exports.RandUtil = {}));
//# sourceMappingURL=rand-util.js.map