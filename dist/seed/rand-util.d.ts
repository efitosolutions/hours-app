export declare namespace RandUtil {
    function getRandElements(list: any[]): any[];
    function getRandomArbitrary(min: any, max: any): number;
    function randomDate(start: any, end: any): Date;
}
