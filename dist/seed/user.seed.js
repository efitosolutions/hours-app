"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const user_dao_1 = require("../dao/user.dao");
exports.adminEmail = `admin@test.com`;
async function seedUsers() {
    const user = {
        password: "sAdmin",
        name: `Super Admin`,
        email: "admin@email.com",
        number: "1234567890",
        role: "admin",
        active: true,
    };
    const user1 = await createUser(user);
    return [user1];
}
exports.default = seedUsers;
async function createUser(user) {
    const existingUser = await user_dao_1.UserDao.getUserByEmail(user.email);
    if (existingUser) {
        return existingUser;
    }
    return await user_dao_1.UserDao.createUser(user);
}
//# sourceMappingURL=user.seed.js.map