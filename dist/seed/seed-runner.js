"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const database_1 = require("../startup/database");
const mongoose = require("mongoose");
const seed_1 = require("./seed");
require('dotenv').config();
async function runSeed() {
    await database_1.default();
    await seed_1.default();
    await mongoose.disconnect();
}
// noinspection JSIgnoredPromiseFromCall
runSeed();
//# sourceMappingURL=seed-runner.js.map